# Quality Assesment of Bruker to Nifti conversion tools

bruker2nifti_qa is a MRI dataset in the Paravision Bruker raw format.

# How to Use

You have to first install [git](https://git-scm.com/book/en/v1/Getting-Started-Installing-Git) (>=1.8) and [git-lfs](https://github.com/git-lfs/git-lfs?utm_source=gitlfs_site&utm_medium=source_link&utm_campaign=gitlfs#installation) (>=[2.1](https://github.com/git-lfs/git-lfs/releases/tag/v2.1.1))

Then you can clone the whole repository at this address:
```shell
git lfs clone https://gitlab.com/naveau/bruker2nifti_qa.git
```

The dataset are described in the yaml files in the raw sub-directories.

# Licence

Images come from different centers :

## `raw/Cyceron_*`

The images were acquired by Mikaël Naveau at the [CYCERON](http://www.cyceron.fr) platform
and are covered by a [CC-BY 4.0 licence](https://creativecommons.org/licenses/by/4.0/).
Please attribute this work to the [UMS3408, CYCERON](http://www.cyceron.fr).

## `raw/McGill_Orientation`

These images were acquired to validate spatial orientation in the Bruker Format by Axel Mathieu and Gabriel Devenyi from the Computational Brain Anatomy Laboratory, Cerebral Imaging Center, Douglas Mental Health University Institute at McGill University. The files are released to the public domain.